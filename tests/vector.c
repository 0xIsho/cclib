#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <stdint.h>

#include <cmocka.h>

#include "cclib/vector.h"

void alloc_and_free_empty()
{
	struct ccl_vector* vec = NULL;
	assert_true(ccl_vector_alloc(&vec, sizeof(int), 0));
	assert_true(ccl_vector_is_empty(vec));
	assert_non_null(vec);
	ccl_vector_free(&vec);
	assert_null(vec);
}

void alloc_and_free_non_empty()
{
	struct ccl_vector* vec = NULL;
	assert_true(ccl_vector_alloc(&vec, sizeof(int), 4));
	assert_true(ccl_vector_is_empty(vec));
	assert_non_null(vec);
	ccl_vector_free(&vec);
	assert_null(vec);
}

void size()
{
	struct ccl_vector* vec = NULL;
	assert_true(ccl_vector_alloc(&vec, sizeof(int), 4));

	assert_int_equal(ccl_vector_size(vec), 0);

	ccl_vector_free(&vec);
}

void capacity()
{
	struct ccl_vector* vec = NULL;
	assert_true(ccl_vector_alloc(&vec, sizeof(int), 4));

	assert_int_equal(ccl_vector_capacity(vec), 4);

	ccl_vector_free(&vec);
}

void is_empty()
{
	struct ccl_vector* vec = NULL;
	assert_true(ccl_vector_alloc(&vec, sizeof(int), 4));

	assert_true(ccl_vector_is_empty(vec));

	int data = 0;
	assert_true(ccl_vector_push_back(vec, &data));
	assert_false(ccl_vector_is_empty(vec));

	ccl_vector_pop_back(vec);
	assert_true(ccl_vector_is_empty(vec));

	ccl_vector_free(&vec);
}

void insert()
{
	struct ccl_vector* vec = NULL;
	assert_true(ccl_vector_alloc(&vec, sizeof(int), 0));

	// The items are inserted in the order 'head', 'tail', then middle in order
	// to test insertions in the middle in addition to both ends. Insertion at
	// 0, 1, 2 will not achieve that, as that's essentially a push_front,
	// push_back, push_back.

	int items[] = { 0, 1, 2 };

	assert_true(ccl_vector_insert(vec, 0, &items[0]));
	assert_int_equal(CCL_UNVOID(int, ccl_vector_get(vec, 0)), items[0]);
	assert_int_equal(ccl_vector_size(vec), 1);

	assert_true(ccl_vector_insert(vec, 1, &items[1]));
	assert_int_equal(CCL_UNVOID(int, ccl_vector_get(vec, 1)), items[1]);
	assert_int_equal(ccl_vector_size(vec), 2);

	assert_true(ccl_vector_insert(vec, 1, &items[2]));
	assert_int_equal(CCL_UNVOID(int, ccl_vector_get(vec, 1)), items[2]);
	assert_int_equal(ccl_vector_size(vec), 3);

	// Make sure the item that used to be at 1 got shifted
	assert_int_equal(CCL_UNVOID(int, ccl_vector_get(vec, 2)), items[1]);

	ccl_vector_free(&vec);
}

void push_back()
{
	struct ccl_vector* vec = NULL;
	assert_true(ccl_vector_alloc(&vec, sizeof(int), 1));

	int items[] = { 0, 1, 2 };

	assert_true(ccl_vector_push_back(vec, &items[0]));
	assert_int_equal(ccl_vector_size(vec), 1);

	assert_true(ccl_vector_push_back(vec, &items[1]));
	assert_int_equal(ccl_vector_size(vec), 2);

	assert_true(ccl_vector_push_back(vec, &items[2]));
	assert_int_equal(ccl_vector_size(vec), 3);

	assert_int_equal(CCL_UNVOID(int, ccl_vector_get(vec, 0)), items[0]);
	assert_int_equal(CCL_UNVOID(int, ccl_vector_get(vec, 1)), items[1]);
	assert_int_equal(CCL_UNVOID(int, ccl_vector_get(vec, 2)), items[2]);

	ccl_vector_free(&vec);
}

void remove()
{
	struct ccl_vector* vec = NULL;
	assert_true(ccl_vector_alloc(&vec, sizeof(int), 1));

	int items[] = { 0, 1, 2, 3 };
	assert_true(ccl_vector_push_back(vec, &items[0]));
	assert_true(ccl_vector_push_back(vec, &items[1]));
	assert_true(ccl_vector_push_back(vec, &items[2]));
	assert_true(ccl_vector_push_back(vec, &items[3]));
	assert_int_equal(ccl_vector_size(vec), 4);

	assert_int_equal(CCL_UNVOID(int, ccl_vector_get(vec, 0)), items[0]);
	assert_int_equal(CCL_UNVOID(int, ccl_vector_get(vec, 1)), items[1]);
	assert_int_equal(CCL_UNVOID(int, ccl_vector_get(vec, 2)), items[2]);
	assert_int_equal(CCL_UNVOID(int, ccl_vector_get(vec, 3)), items[3]);

	// The order of removal is kind of important here. It checks removing from
	// the beginning, middle, end and removing the only item in the vector.
	// Make sure this holds when editing the tests.

	ccl_vector_remove(vec, 0);
	assert_int_equal(ccl_vector_size(vec), 3);
	assert_int_equal(CCL_UNVOID(int, ccl_vector_get(vec, 0)), items[1]);
	assert_int_equal(CCL_UNVOID(int, ccl_vector_get(vec, 1)), items[2]);
	assert_int_equal(CCL_UNVOID(int, ccl_vector_get(vec, 2)), items[3]);

	ccl_vector_remove(vec, 1);
	assert_int_equal(ccl_vector_size(vec), 2);
	assert_int_equal(CCL_UNVOID(int, ccl_vector_get(vec, 0)), items[1]);
	assert_int_equal(CCL_UNVOID(int, ccl_vector_get(vec, 1)), items[3]);

	ccl_vector_remove(vec, 1);
	assert_int_equal(ccl_vector_size(vec), 1);
	assert_int_equal(CCL_UNVOID(int, ccl_vector_get(vec, 0)), items[1]);

	ccl_vector_remove(vec, 0);
	assert_int_equal(ccl_vector_size(vec), 0);

	ccl_vector_free(&vec);
}

void pop_back()
{
	struct ccl_vector* vec = NULL;
	assert_true(ccl_vector_alloc(&vec, sizeof(int), 1));

	int items[] = { 0, 1, 2, 3 };
	assert_true(ccl_vector_push_back(vec, &items[0]));
	assert_true(ccl_vector_push_back(vec, &items[1]));
	assert_true(ccl_vector_push_back(vec, &items[2]));
	assert_true(ccl_vector_push_back(vec, &items[3]));

	ccl_vector_pop_back(vec);
	assert_int_equal(CCL_UNVOID(int, ccl_vector_get(vec, 0)), 0);
	assert_int_equal(CCL_UNVOID(int, ccl_vector_get(vec, 1)), 1);
	assert_int_equal(CCL_UNVOID(int, ccl_vector_get(vec, 2)), 2);
	assert_int_equal(ccl_vector_size(vec), 3);

	ccl_vector_pop_back(vec);
	assert_int_equal(CCL_UNVOID(int, ccl_vector_get(vec, 0)), 0);
	assert_int_equal(CCL_UNVOID(int, ccl_vector_get(vec, 1)), 1);
	assert_int_equal(ccl_vector_size(vec), 2);

	ccl_vector_pop_back(vec);
	assert_int_equal(CCL_UNVOID(int, ccl_vector_get(vec, 0)), 0);
	assert_int_equal(ccl_vector_size(vec), 1);

	ccl_vector_pop_back(vec);
	assert_int_equal(ccl_vector_size(vec), 0);

	ccl_vector_free(&vec);
}

void clear()
{
	struct ccl_vector* vec = NULL;
	assert_true(ccl_vector_alloc(&vec, sizeof(int), 1));

	int data = 0;

	assert_true(ccl_vector_push_back(vec, &data));
	assert_false(ccl_vector_is_empty(vec));

	ccl_size_t old_cap = ccl_vector_capacity(vec);
	ccl_vector_clear(vec);
	assert_true(ccl_vector_is_empty(vec));
	// 'clear' should not reallocate the vector
	assert_int_equal(ccl_vector_capacity(vec), old_cap);

	ccl_vector_free(&vec);
}

void compact()
{
	struct ccl_vector* vec = NULL;
	assert_true(ccl_vector_alloc(&vec, sizeof(int), 4));

	int data = 0;
	assert_true(ccl_vector_push_back(vec, &data));
	assert_true(ccl_vector_push_back(vec, &data));
	assert_true(ccl_vector_capacity(vec) > ccl_vector_size(vec));

	assert_true(ccl_vector_compact(vec));
	assert_true(ccl_vector_capacity(vec) == ccl_vector_size(vec));

	ccl_vector_clear(vec);
	assert_true(ccl_vector_compact(vec));

	assert_true(ccl_vector_capacity(vec) == 0);
	assert_true(ccl_vector_size(vec) == 0);

	assert_true(ccl_vector_push_back(vec, &data));
	assert_true(ccl_vector_capacity(vec) >= ccl_vector_size(vec));

	ccl_vector_free(&vec);
}

int main(void)
{
	const struct CMUnitTest tests[] = {
		cmocka_unit_test(alloc_and_free_empty),
		cmocka_unit_test(alloc_and_free_non_empty),
		cmocka_unit_test(size),
		cmocka_unit_test(capacity),
		cmocka_unit_test(is_empty),
		cmocka_unit_test(insert),
		cmocka_unit_test(push_back),
		cmocka_unit_test(remove),
		cmocka_unit_test(pop_back),
		cmocka_unit_test(clear),
		cmocka_unit_test(compact),
	};

	return cmocka_run_group_tests(tests, NULL, NULL);
}
