set(CCL_TESTS
	vector.c
)

foreach(test ${CCL_TESTS})
	# Get file extension
	get_filename_component(TEST_EXT ${test} EXT)

	# Remove file extension
	string(REPLACE ${TEST_EXT} "" TEST_NAME ${test})

	string(PREPEND ${TEST_NAME} "test_" TEST_NAME)

	set(TEST_TARGET ${TEST_NAME})

	add_executable(${TEST_TARGET} ${test})
	target_link_libraries(${TEST_TARGET} cclib cmocka)
	add_test(NAME ${TEST_TARGET} COMMAND ${TEST_TARGET})
endforeach()
