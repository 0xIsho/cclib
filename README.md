# cclib

*C* *C*ontainers *Lib*rary is a generic data-structures library written in C.

## Features

- [x] Dynamic Array/Vector
- [ ] Stack
- [ ] Queue
- [ ] Deque (Double-Ended Queue)
- [ ] Lists
  - [ ] Singly Linked List
  - [ ] Doubly Linked List
- [ ] Hash Table
- [ ] Hash Set

## Build

This project uses CMake as the build system. To build the library:

```sh
# Clone the repository
git clone https://gitlab.com/0xIsho/cclib.git && cd cclib

# Configure
cmake -S . -B out/build

# Build
cmake --build out/build

# Test
ctest --test-dir out/build
```

## License

This project is licensed under the Apache License Version 2.0. See [LICENSE](LICENSE) for details.

