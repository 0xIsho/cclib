#include "cclib/vector.h"

#include <stdlib.h>
#include <string.h>

struct ccl_vector {
	ccl_byte_t* data;     /**< Pointer to managed buffer */
	ccl_size_t elem_size; /**< The element size in bytes */
	ccl_size_t size;      /**< Size (valid elements) of the managed buffer in bytes */
	ccl_size_t cap;       /**< Capacity (size + free space) of the managed buffer in bytes */
};

bool ccl_vector_alloc(struct ccl_vector** self, ccl_size_t elem_size, ccl_size_t initial_capacity)
{
	CCL_ASSERT(self != NULL);
	CCL_ASSERT(initial_capacity >= 0);
	CCL_ASSERT(elem_size > 0);

	struct ccl_vector* vec = malloc(sizeof(struct ccl_vector));

	if (vec == NULL) {
		return false;
	}

	if (initial_capacity > 0) {
		vec->data = malloc(initial_capacity * elem_size);

		if (vec->data == NULL) {
			free(vec);
			return false;
		}
	} else {
		vec->data = NULL;
	}

	vec->elem_size = elem_size;
	vec->size = 0;
	vec->cap = initial_capacity * elem_size;
	*self = vec;

	return true;
}

void ccl_vector_free(struct ccl_vector** self)
{
	CCL_ASSERT(self != NULL);

	if (*self == NULL) {
		return;
	}

	free((*self)->data);
	free(*self);
	*self = NULL;
}

ccl_size_t ccl_vector_size(struct ccl_vector* self)
{
	CCL_ASSERT(self != NULL);

	return self->size / self->elem_size;
}

ccl_size_t ccl_vector_capacity(struct ccl_vector* self)
{
	CCL_ASSERT(self != NULL);

	return self->cap / self->elem_size;
}

bool ccl_vector_is_empty(struct ccl_vector* self)
{
	CCL_ASSERT(self != NULL);

	return ccl_vector_size(self) == 0;
}

const void* ccl_vector_get(struct ccl_vector* self, ccl_index_t which)
{
	CCL_ASSERT(self != NULL);
	CCL_ASSERT(which >= 0);
	CCL_ASSERT(which < ccl_vector_size(self));

	return self->data + (which * self->elem_size);
}

bool ccl_vector_insert(struct ccl_vector* self, ccl_index_t where, void* item)
{
	CCL_ASSERT(self != NULL);
	CCL_ASSERT(item != NULL);
	CCL_ASSERT(where >= 0);
	CCL_ASSERT(where <= ccl_vector_size(self));

	if (ccl_vector_size(self) == ccl_vector_capacity(self)) {
		const size_t cap_new = (self->cap == 0 ? self->elem_size : self->cap) * 2;
		ccl_byte_t* data_new = realloc(self->data, cap_new);

		if (data_new == NULL) {
			return false;
		}

		self->data = data_new;
		self->cap = cap_new;
	}

	ccl_byte_t* dst_addr = NULL;

	if (where == 0) {
		dst_addr = self->data;
		memmove(self->data + self->elem_size, self->data, self->size);
	} else if (where == ccl_vector_size(self)) {
		dst_addr = self->data + self->size;
	} else {
		ccl_size_t offset = where * self->elem_size;
		dst_addr = self->data + offset;
		memmove(dst_addr + self->elem_size, dst_addr, self->size - offset);
	}

	CCL_ASSERT(dst_addr != NULL);
	memcpy(dst_addr, item, self->elem_size);

	self->size += self->elem_size;

	return true;
}

bool ccl_vector_push_back(struct ccl_vector* self, void* item)
{
	return ccl_vector_insert(self, ccl_vector_size(self), item);
}

void ccl_vector_remove(struct ccl_vector* self, ccl_index_t which)
{
	CCL_ASSERT(self != NULL);
	CCL_ASSERT(which >= 0);
	CCL_ASSERT(which < ccl_vector_size(self));
	CCL_ASSERT(ccl_vector_size(self) > 0); // Probably redundant given the above checks

	if (which != ccl_vector_size(self)) {
		const ccl_size_t offset = which * self->elem_size;
		ccl_byte_t* dst_addr = self->data + offset;
		memmove(dst_addr, dst_addr + self->elem_size, self->size - offset);
	}

	self->size -= self->elem_size;
}

void ccl_vector_pop_back(struct ccl_vector* self)
{
	self->size -= self->elem_size;
}

void ccl_vector_clear(struct ccl_vector *self)
{
	CCL_ASSERT(self != NULL);

	self->size = 0;
}

bool ccl_vector_compact(struct ccl_vector *self)
{
	CCL_ASSERT(self != NULL);

	if (self->size == self->cap) {
		return true;
	}

	ccl_byte_t* data = realloc(self->data, self->size);

	if (data == NULL && self->size > 0) {
		return false;
	}

	self->data = data;
	self->cap = self->size;

	return true;
}
