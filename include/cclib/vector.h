#ifndef CCLIB_VECTOR_H
#define CCLIB_VECTOR_H

#include <stdbool.h>

#include "cclib/common.h"

struct ccl_vector;

/**
 * @brief      Allocate a new vector with the specified element capacity
 *
 * This function allocates a new vector that's able to hold `initialCapacity`
 * elements without allocating more memory. The vector is still able to expand
 * on demand.
 *
 * The output vector (`self`) is left untouched and the function cleans up after itself on failure.
 *
 * @param[out]  self              The allocated vector
 * @param[in]   elem_size         The size of a single element in bytes. MUST be > 0
 * @param[in]   initial_capacity  The initial capacity of the vector
 *
 * @return     true on success. false otherwise.
 */
bool ccl_vector_alloc(struct ccl_vector** self, ccl_size_t elem_size, ccl_size_t initial_capacity);

/**
 * @brief      Release an allocated vector
 *
 * This function releases an allocated vector. The provided pointer is set to
 * NULL. Nothing is done if the pointer is already NULL.
 *
 * @param      self  The vector to be released
 */
void ccl_vector_free(struct ccl_vector** self);

/**
 * @brief      Query the number of elements contained in the vector
 *
 * @param      self  The vector to be queried
 *
 * @return     The number of elements contained in the vector
 *
 * @note       `self` MUST point to a valid vector.
 */
ccl_size_t ccl_vector_size(struct ccl_vector* self);

/**
 * @brief      Query the capacity of the vector
 *
 * The capacity is the maximum number of element the vector can hold without
 * needing to allocate additional memory.
 *
 * This holds true: `ccl_vector_capacity(vec) >=ccl_vector_size(vec)`. The
 * capacity being greater than the size means the vector has some unused (free)
 * space.
 *
 * @param      self  The vector to be queried
 *
 * @return     The capacity of the vector
 *
 * @note       `self` MUST point to a valid vector.
 */
ccl_size_t ccl_vector_capacity(struct ccl_vector* self);

/**
 * @brief      Check if the vector is empty
 *
 * This function checks if the specified vector is empty (size == 0).
 *
 * @param      self  The vector to be checked
 *
 * @return     true if the vector is empty. false otherwise.
 */
bool ccl_vector_is_empty(struct ccl_vector* self);

/**
 * @brief      Retrieve an item from the vector
 *
 * @param      self   The vector to be queried
 * @param[in]  which  The index of the item to be retrieved
 *
 * @return     A pointer to the item
 *
 * @note       The returned pointer may get invalidated by insertion and
 *             deletion operations and because of that, it's not recommended to
 *             cache it.
 */
const void* ccl_vector_get(struct ccl_vector* self, ccl_index_t which);

/**
 * @brief      Insert an item into the vector
 *
 * This function inserts the specified item into the vector at the specified
 * position. The vector is expanded as necessary.
 *
 * Inserting into the vector works by shifting the affected items (`>= where`)
 * to the right and inserting the new item at `where`. For example inserting
 * the value 4 into the vector [0,1,2,3] at index 2 will result in [0,1,4,2,3]
 *
 * Inserting at 0 puts the item at the head of the vector. Inserting at the
 * size of the vector (`where == ccl_vector_size(self)`) puts the item at the
 * tail.
 *
 * `self` is left untouched if the function fails.
 *
 * @param      self   The vector to insert into
 * @param[in]  where  The index at which to insert the item. MUST be
 *                    `< ccl_vector_size(self)`
 * @param      item   The item to be inserted
 *
 * @return     true on success. false otherwise.
 */
bool ccl_vector_insert(struct ccl_vector* self, ccl_index_t where, void* item);

/**
 * @brief      Append an item to the end of the vector
 *
 * This function inserts an item at the end of the vector. The vector is
 * expanded as necessary.
 *
 * `self` is left untouched if the function fails.
 *
 * @param      self  The vector to append to
 * @param      item  The item to be appended
 *
 * @return     true on success. false otherwise.
 */
bool ccl_vector_push_back(struct ccl_vector* self, void* item);

/**
 * @brief      Remove an item from the vector
 *
 * This function removes the item at the specified index from the vector. The
 * size is decremented by 1 and the capacity is unaffected. This function does
 * not trigger a reallocation.
 *
 * There MUST be at least 1 item in the vector when calling this function!
 *
 * @param      self   The vector to remove the item from
 * @param[in]  which  The index of the item to be removed
 */
void ccl_vector_remove(struct ccl_vector* self, ccl_index_t which);

/**
 * @brief      Remove the item at the end of the vector
 *
 * This function removes the last item in the vector. The function does not
 * return the item, it should be copied out of the vector if necessary.
 *
 * @param      self  The vector to remove the item from
 */
void ccl_vector_pop_back(struct ccl_vector* self);

/**
 * @brief      Clear the vector
 *
 * This function clears the vector; meaning all the elementes contained within
 * are removed.
 *
 * Note that the capacity of the vector is unaffected; meaning allocated memory
 * *does not* get released! Use ccl_vector_compact() after a call to this
 * function in order to release unused memory if that is desired.
 *
 * @param      self  The vector to be cleared
 */
void ccl_vector_clear(struct ccl_vector* self);

/**
 * @brief      Release unused memory
 *
 * This function releases all the unused memory allocated by the vector.
 * After a call to this function the vector's capacity will equal its size.
 *
 * Keep in mind that unused memory might be useful when inserting new elements
 * into the vector, as that means no new (re-)allocations will need to be done.
 *
 * @param      self  The object
 *
 * @return     true on success. false otherwise.
 */
bool ccl_vector_compact(struct ccl_vector* self);

#endif // CCLIB_VECTOR_H
