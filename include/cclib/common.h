#ifndef CCLIB_COMMON_H
#define CCLIB_COMMON_H

#include <stdint.h>
#include <assert.h>

#define CCL_ASSERT(x) assert(x)

// Casts a pointer-to-void 'v' back to a value type 't'. e.g. void* -> int
#define CCL_UNVOID(t, v) (*(t*)v)

#if defined __has_include
#	if __has_include(<stdbool.h>)
#		include <stdbool.h>
#	endif
#endif

#ifndef bool
#	define bool int
#	define true 1
#	define false 0
#endif

typedef signed long ccl_size_t;
typedef uint8_t ccl_byte_t;
typedef ccl_size_t ccl_index_t;

#endif // CCLIB_COMMON_H
